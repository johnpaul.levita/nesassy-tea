import { Injectable } from '@angular/core';
import { Food } from '../models/food.model';

@Injectable({
  providedIn: 'root',
})
export class FoodService {
  getFoods(): Food[] {
    return [
      {
        id: 1,
        title: 'Bacon&Beef',
        price: 130,
        image: 'assets/images/foods/bacon.jpg',
        description:
          'Fresh veggies, angus pure beef patty, quickmelt cheese, bbq sauce, glaced onion, egg mayo and crispy bacon served with sweet and cheesy fries.',
      },
      {
        id: 2,
        title: 'BBQEdition',
        price: 105,
        image: 'assets/images/foods/bbq.jpg',
        description:
          'Fresh veggies, angus pure beef patty, cheese, bbq sauce, glaced onion, and egg mayo served with sweet and cheesy fries',
      },
      {
        id: 3,
        title: 'Cheesy PurePatty',
        price: 120,
        image: 'assets/images/foods/cheesy.jpg',
        description:
          'Fresh Lettuce, Tomato, Angus, Pure Beef Patty, Double quikmelt cheese, Bbq sauce, Glazed Onion, Egg Mayo, Served with sweet and cheesy Fries',
      },
      {
        id: 4,
        title: 'OreoClassic',
        price: 120,
        image: 'assets/images/foods/oreo.jpg',
        description:
          'Oreo cookies and cream ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 5,
        title: 'Lychee YakultSeries',
        price: 110,
        image: 'assets/images/foods/lychee.jpg',
        description:
          'Lychee and Yakult ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 6,
        title: 'FourSeasons YakultSeries',
        price: 110,
        image: 'assets/images/foods/4sns.jpg',
        description:
          'Mango, orange, pineapple and guava ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 7,
        title: 'GreenApple YakultSeries',
        price: 110,
        image: 'assets/images/foods/gapple.jpg',
        description:
          'Green apple and Yakult ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 8,
        title: 'Blue Lemonade',
        price: 110,
        image: 'assets/images/foods/blemonade.jpg',
        description:
          'Blue curacao, lemon syrup and yakult  ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 9,
        title: 'Choco Berry',
        price: 130,
        image: 'assets/images/foods/chberry.jpg',
        description:
          'Chocolate and black berry ingredients that suits well when mixed with a tea base. It has a creamy texture that customers will surely love. ',
      },
      {
        id: 10,
        title: 'Dark Choco',
        price: 130,
        image: 'assets/images/foods/dcc.jpg',
        description:
          'Dark chocolate ingredient that suits well when mixed with a tea base. It has a creamy texture that customers will surely love.',
      },
      {
        id: 11,
        title: 'Onion Rings',
        price: 105,
        image: 'assets/images/foods/onion.jpg',
        description:
          'A ring of sliced onion coated with batter or crumbs and fried.',
      },
      {
        id: 12,
        title: 'Fries',
        price: 105,
        image: 'assets/images/foods/frs.jpg',
        description:
          'A thin strip of potato, usually cut 3 to 4 inches in length and about 1/4 to 3/8 inches square that are deep fried until they are golden brown and crisp textured on the outside while remaining white and soft on the inside.',
      },
    ];
  }

  getFood(id: number): Food {
    return this.getFoods().find((food) => food.id === id);
  }
}
